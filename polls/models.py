from django.db import models
import datetime
from django.utils import timezone


class Client(models.Model):
    nom = models.CharField(max_length=50)
    prenom = models.CharField(max_length=100)
    adresse = models.CharField(max_length=200)
    telephone = models.CharField(max_length=200)
    photo = models.CharField(max_length=200)

    def __str__(self):
        return self.nom, self.prenom, self.adresse, self.telephone, self.photo


class Lunette(models.Model):
    nom = models.CharField(max_length=50)
    types = models.CharField(max_length=100)
    prix = models.CharField(max_length=100)
    photo = models.CharField(max_length=200)

    def __str__(self):
        return self.nom, self.types, self.prix, self.photo


class Commande(models.Model):
    client_id = models.CharField(max_length=50)
    lunette_id = models.CharField(max_length=100)
    date = models.DateTimeField('date Commande')
    nbre_lunette = models.CharField(max_length=200)
    montant_total = models.CharField(max_length=200 )

    def __str__(self):
        return self.client_id, self.lunette_id, self.date, self.nbre_lunette, self.montant_total

    def was_commande_recently(self):
        return self.commande_date >= timezone.now() - datetime.timedelta(days=1)