from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('admin/polls/client/', include('polls.urls')),
     path('admin/polls/lunette/', include('polls.urls')),
      path('admin/polls/commande/', include('polls.urls')),
    path('polls/', include('polls.urls')),
    path('admin/', admin.site.urls),
]